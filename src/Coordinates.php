<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2019-01-31
 * Time: 14:58
 */

namespace Googlemaps;

use Googlemaps\Coordinates\Result;
use Googlemaps\Http\Requester;

class Coordinates
{
    protected $requester;

    public function __construct(Requester $requester){
        $this->requester = $requester;
    }

    public function query(Coordinates\QueryInterface $query) : Coordinates\Result {
        $response = $this->requester->request($query);
        if(200 === $response->getResponseStatus()){

            $geolocationResult = json_decode($response->getResponseBody());
            $lat = $geolocationResult->results[0]->geometry->location->lat;
            $lng = $geolocationResult->results[0]->geometry->location->lng;

            $result = new Result();
            $result->setLatitude($lat);
            $result->setLongitude($lng);
            return $result;

        } else {
            return new Result();
        }
    }
}