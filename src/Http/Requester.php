<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2019-02-01
 * Time: 09:38
 */

namespace Googlemaps\Http;


use Googlemaps\Coordinates\QueryInterface;

interface Requester
{

    const METHOD_GET = "GET";
    const METHOD_POST = "POST";

    public function request(QueryInterface $query) : Response;
}