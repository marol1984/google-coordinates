<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2019-02-01
 * Time: 10:12
 */

namespace Googlemaps\Http\Guzzle;


class Response implements \Googlemaps\Http\Response
{

    protected $body;
    protected $httpCode;


    public function __construct(\GuzzleHttp\Psr7\Response $response){
        $this->httpCode = $response->getStatusCode();
        $this->body = $response->getBody();
    }

    public function getResponseBody()
    {
        return $this->body;
    }

    public function getResponseStatus()
    {
        return $this->httpCode;
    }

}