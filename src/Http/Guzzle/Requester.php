<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2019-02-01
 * Time: 09:47
 */

namespace Googlemaps\Http\Guzzle;


use Googlemaps\Coordinates\QueryInterface;
use Googlemaps\Http\Requester as RequesterInterface;


class Requester implements RequesterInterface
{

    protected $client;

    public function __construct(\GuzzleHttp\Client $client)
    {
        $this->client = $client;
    }

    public function request(QueryInterface $query): \Googlemaps\Http\Response
    {
        $url = "https://maps.google.com/maps/api/geocode/json?address=" . urlencode($query->getQueryString()) . "&sensor=false&key=".urlencode($query->getApiKey());
        $response = new \Googlemaps\Http\Guzzle\Response( $this->client->request(\Googlemaps\Http\Requester::METHOD_GET, $url) );
        return $response;
    }


}