<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2019-01-31
 * Time: 14:59
 */

namespace Googlemaps\Http;


Interface Response
{
    public function getResponseBody();
    public function getResponseStatus();
}