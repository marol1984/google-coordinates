<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2019-02-01
 * Time: 12:03
 */

namespace Googlemaps\Coordinates;


interface QueryInterface
{
    public function getQueryString();
    public function getApiKey();
}