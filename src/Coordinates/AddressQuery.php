<?php
/**
 * Created by PhpStorm.
 * User: g50
 * Date: 2019-02-01
 * Time: 12:04
 */

namespace Googlemaps\Coordinates;


class AddressQuery implements QueryInterface
{
    protected $address;
    protected $apiKey;

    public function __construct($address, $apiKey)
    {
        $this->address = $address;
        $this->apiKey = $apiKey;
    }

    public function getQueryString()
    {
        return $this->address;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

}